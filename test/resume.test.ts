import { Resume } from '../src/types'
import { create, is } from 'superstruct'

it(`should have the base resume schema`, async () => {
  const fake = create(
    {
      basics: {
        name: 'Tyler',
        email: 'someone@somewhere.com',
      },
    },
    Resume
  )

  expect(fake.basics.name).toBe('Tyler')
  expect(fake.basics.email).toBe('someone@somewhere.com')
  expect(is(fake, Resume)).toBeTruthy()
})
