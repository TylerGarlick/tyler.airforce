import * as React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'gatsby'

import styled from 'styled-components'

const BrandContainer = styled.div`
  display: flex;
  border: 1px solid red;
`

const TopNavigation = styled.header``

const Header = ({ siteTitle }) => (
  <TopNavigation>
    <BrandContainer>
      <h1 style={{ margin: 0 }}>
        <Link
          to="/"
          style={{
            color: `white`,
            textDecoration: `none`,
          }}
        >
          tyler.airforce
        </Link>
      </h1>
    </BrandContainer>

    {/* <div
      style={{
        margin: `0 auto`,
        maxWidth: 960,
        padding: `1.45rem 1.0875rem`,
      }}
    >

    </div> */}
  </TopNavigation>
)

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
