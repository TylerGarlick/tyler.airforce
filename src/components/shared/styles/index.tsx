import styled from 'styled-components'
import {} from 'grommet'

export const Form = styled.form`
  margin: 0;
  padding: 0;
`

export const BasicForm = styled(Form)`
  padding: 8px;
  margin: 8px;
`


export const Row = styled.section`
  padding: 8px;
  display: flex;
`

export const FormActions = styled(Row)`

`

export const Label = styled.label``

// export const
