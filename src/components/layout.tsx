import * as React from 'react'
import PropTypes from 'prop-types'
import { useStaticQuery, graphql } from 'gatsby'

// import './layout.css'
import './shared/styles/normalize.css'
import './shared/styles/skeleton.css'
import styled from 'styled-components'
import {
  Box,
  Text,
  Header,
  Grommet,
  Nav,
  Anchor,
  Main as GrommetMain,
} from 'grommet'

// const Header = styled.header``
const Main = styled(GrommetMain)`
  display: flex;
  flex-direction: column;
  height: 100%;
  width: 100vw;
`
const Footer = styled.footer`
  background: black;
  color: white;
  a {
    color: white;
  }
`
const TopNavigation = styled(Header)`
  background: #e6e6e6;
`

const Layout = ({ children }) => {
  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          title
        }
      }
    }
  `)

  return (
    <Grommet full>
      <Box fill>
        <TopNavigation pad="large" elevation="medium">
          <Anchor label="tyler.airforce" href="/" />
          <Nav direction="row">
            <Anchor label="Resume" href="/resume" />
            <Anchor label="Contact" href="/contact" />
          </Nav>
        </TopNavigation>
        <Main pad="large">{children}</Main>
        <Footer>
          <Box pad="large">
            © {new Date().getFullYear()}{' '}
            <Anchor label="tyler.airforce" href="/" />
          </Box>
        </Footer>
      </Box>
    </Grommet>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
