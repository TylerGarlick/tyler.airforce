import * as React from 'react'
import { Link } from 'gatsby'
import { StaticImage } from 'gatsby-plugin-image'
import { Box } from 'grommet'
import Layout from '../components/layout'
import Seo from '../components/seo'

const IndexPage = () => (
  <Layout>
    <Seo title="Home" />
    <Box pad="medium">
      <h1>Testing</h1>
      <p>test 123</p>
    </Box>
  </Layout>
)

export default IndexPage
