import {
  object,
  array,
  optional,
  string,
  date,
  refine,
  number,
  enums,
} from 'superstruct'

import isEmail from 'is-email'
import isUrl from 'is-url'

const Highlights = array(string())
const Keywords = array(string())
const Email = refine(string(), 'email', isEmail)
const Url = refine(string(), 'url', isUrl)

export const Resume = object({
  basics: object({
    name: string(),
    label: optional(string()),
    image: optional(string()),
    email: Email,
    phone: optional(string()),
    url: optional(Url),
    summary: optional(string()),
    location: optional(
      object({
        address: optional(string()),
        postalCode: optional(string()),
        city: optional(string()),
        countryCode: optional(string()),
        region: optional(string()),
      })
    ),
    profiles: optional(
      array(
        object({
          network: optional(string()),
          username: optional(string()),
          url: optional(Url),
        })
      )
    ),
  }),
  work: optional(
    array(
      object({
        name: string(),
        location: optional(string()),
        description: optional(string()),
        url: optional(Url),
        startDate: optional(date()),
        endDate: optional(date()),
        summary: optional(string()),
        highlights: optional(Highlights),
      })
    )
  ),
  volunteer: optional(
    array(
      object({
        organization: string(),
        volunteer: string(),
        url: string(),
        startDate: optional(date()),
        endDate: optional(date()),
        summary: optional(string()),
        highlights: optional(Highlights),
      })
    )
  ),
  education: optional(
    array(
      object({
        institution: string(),
        url: Url,
        area: optional(string()),
        studyType: optional(string()),
      })
    )
  ),
  awards: optional(
    array(
      object({
        title: string(),
        date: optional(date()),
        awarder: optional(string()),
        summary: string(),
        startDate: optional(date()),
        endDate: optional(date()),
        score: number(),
        courses: optional(array(string())),
      })
    )
  ),
  publications: optional(
    array(
      object({
        name: string(),
        publisher: optional(string()),
        releaseDate: optional(date()),
        awarder: optional(string()),
        summary: string(),
      })
    )
  ),
  skills: optional(
    array(
      object({
        name: string(),
        level: optional(string()),
        keywords: Keywords,
      })
    )
  ),
  languages: optional(
    array(
      object({
        language: optional(string()),
        fluency: optional(string()),
      })
    )
  ),
  interests: optional(
    array(
      object({
        name: string(),
        keywords: Keywords,
      })
    )
  ),
  references: optional(
    array(
      object({
        name: string(),
        reference: optional(string()),
      })
    )
  ),
  projects: optional(
    array(
      object({
        name: string(),
        description: optional(string()),
        highlights: optional(Highlights),
        keywords: optional(Keywords),
        startDate: optional(date()),
        endDate: optional(date()),
        url: optional(Url),
        roles: optional(array(string())),
        entity: optional(string()),
        type: optional(
          enums(['application', 'web', 'mobile', 'native', 'cli', 'ui', 'ux'])
        ),

        /**
         * Additions
         */
        resources: optional(array(Url)),
      })
    )
  ),
})
